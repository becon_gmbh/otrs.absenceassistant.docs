##################
Requirements
##################

OTRS Version Framework 6

- Date::Calc (6.x)
- Time::ParseDate
- Date::Format (1.x)
- MIME::Lite (3.x)
- Net::SMTP
- Digest::MD5 (2.x)
- Date::Holidays::DE
- DateTime::Event::Recurrence