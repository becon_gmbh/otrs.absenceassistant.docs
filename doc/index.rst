##################
Welcome to the OTRS Absence Assistant manuals
##################

This OTRS package includes an absence assistant. An user can request an absence like vacation to his leader. After that the leader will be automatically informed via mail. After the supervisor decide if request is approved or rejected, a message will sent to hr and requester. hr also have this own administration site to manage and configure the absence assistant.

------------

This documentation is organized into a couple of sections:

* :ref:`about-docs`
* :ref:`installation-docs`
* :ref:`usage-docs`

.. _about-docs:

.. toctree::
    :maxdepth: 2
    :caption: About us

    about

.. installation-docs:

.. toctree::
    :maxdepth: 2
    :caption: Getting Started

    architecture
    requirements
    installation
    usage

.. usage-docs:

.. toctree::
    :maxdepth: 2
    :caption: Usage

    users
    hr

##################
License
##################

`becon`_ © 2013-2019 becon GmbH

.. _becon: LICENSE.html