##################
Architecture
##################

This package developed with the core functionalities of OTRS. Files were added without touching the core files of OTRS. So the highest degree of upgradeability is guaranteed. 

.. image:: img/architecture_absence_assistant.png
    :height: 150px
    :alt: alternate text
    :align: center