##################
HR
##################

How to manage employees?
"""""""""""""""""

Go to "Absence -> Manage employee" in menu bar.

.. image:: img/manage_employee.png
    :alt: manage employee
    :align: center


How to get an overview of all vacation accounts?
"""""""""""""""""

Go to "Absence -> Reports" in menu bar.

.. image:: img/reports.png
    :alt: manage employee
    :align: center

Is it possible to set out of office time (otrs functionality) automatically?
"""""""""""""""""

Yes it is. Please just configure the script (/opt/otrs/bin/otrs.VacSetOutOfOffice.pl in crontab. It is necessary to run the script each day. Prefered time is 1 o'clock.
