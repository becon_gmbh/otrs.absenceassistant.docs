##################
Installation
##################

Log in to your OTRS environment as an admin. Now go to the tab

Admin->Paket-Verwaltung

Upload the provided package here.( BeconAbsenceAssistant.opm ).

.. image:: img/paketmanager.jpg
    :height: 200px
    :alt: home screen
    :align: center

After installation you will find a new menu entry.

.. image:: img/menu_overview.png
    :alt: menu overview 
    :align: center

Also you find a new dashboard widget, called absence calendar.
There are three different types of absence: Ill, MAZ and vacation

.. image:: img/dashboard_widget.png
    :height: 200px
    :alt: dashboard widget
    :align: center

Go to the sysconfig (Core -> Absence) to configure the main parts.

.. image:: img/sysconfig.png
    :alt: sysconfig
    :align: center


