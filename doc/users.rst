##################
Users
##################

How to request an absence?
"""""""""""""""""

Go to the dashboard widget and use one of the types of absence: Ill, MAZ or Vacation

.. image:: img/dashboard_widget.png
    :height: 200px
    :alt: dashboard widget
    :align: center


**Ill**

Select Ill to request illness.

.. image:: img/ill_overview.png
    :alt: ill overview
    :width: 49 %

.. image:: img/ill_overview2.png
    :alt: ill overview
    :width: 49 %

Select the day (s) to which you want to request your illness. Do not forget to leave a note. Then click on the green hook to send the absence request.


**MAZ**

Select MAZ to request vacation from your overtime account.

.. image:: img/ill_overview.png
    :alt: maz overview
    :width: 49 %

.. image:: img/ill_overview2.png
    :alt: maz overview
    :width: 49 %

Select the day (s) to which you want to request vacation from your overtime account. Do not forget to leave a note. Then click on the green hook to send the absence request.


**Vacation**

Select Vacation to request vacation.

.. image:: img/vacation_overview.png
    :alt: vacation overview
    :width: 49 %

.. image:: img/vacation_overview2.png
    :alt: vacation overview
    :width: 49 %

Select the day (s) to which you want to request vacation. Do not forget to leave a note. Then click on the green hook to send the absence request.


How to see the process of my requests?
"""""""""""""""""

Go to "Absence -> Manage absence requests" in menu bar.

.. image:: img/myabsence_overview.png
    :alt: my absence overview
    :align: center    
    :width: 60 %

See here the current status of your leave requests. An absence can be stopped before the start of their appearance. To do this, click on the current request and click on the "cancel absence" button.

.. image:: img/myabsence_overview2.png
    :alt: my absence overview
    :align: center
    :width: 60 %


How to see the my current vacation account?
"""""""""""""""""
Go to "Absence -> Overview" in menu bar. On the left side you will find a widget box.

.. image:: img/vacation_account.png
    :alt: vacation overview
    :align: center
    :width: 35 %


How to get an overview of other employees?
"""""""""""""""""
Go to "Absence -> Overview" in menu bar. On the left side you will find a widget box.

.. image:: img/vacation_overview_of_all.png
    :alt: vacation overview of all
    :align: center
